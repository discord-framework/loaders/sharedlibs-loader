#pragma once

#include <discordframework/export.h>
#include <discordframework/plugins-loader/loader.h>
#include <dlopen.hpp>
#include <map>
#include <memory>

#include <discordframework/services/logger.h>

using DiscordFramework::Services::LoggerService;

namespace DiscordFramework {
class EXPORT_DF LoaderManager {
public:
  /**
   * @brief Construct a new Loader Manager object
   *
   */
  LoaderManager();
  /**
   * @brief Destroy the Loader Manager object
   *
   */
  ~LoaderManager();

  /**
   * @brief Load all loaders from the directory
   *
   * @param service_manager a service manager instance
   */
  void init(ServiceManager *service_manager);

  /**
   * @brief Load all plugins from the directory
   */
  void load_plugins();

private:
  /**
   * @brief All loaders
   *
   */
  std::map<std::string, std::unique_ptr<Loader>> m_loaders;

  /**
   * @brief List of loaders native object
   * Used to manage the loader object handler.
   */
  std::map<std::string, std::unique_ptr<DLOpen>> m_loaders_native;

  /**
   * @brief Get a loader from filesystem
   *
   * @param path Path to the loader binary
   */
  void load(const std::string &path, ServiceManager *service_manager);

  LoggerService *m_logger;
};
} // namespace DiscordFramework
