#include <discordframework/core/bot.h>
#include <discordframework/plugins-loader/loader.h>
#include <dlopen.hpp>

#include <sharedlibs-loader/plugin.h>

#include <map>
#include <memory>
#include <string>

using DiscordFramework::Bot;

class SharedLibsLoader : public DiscordFramework::Loader {
public:
  SharedLibsLoader(Bot *bot);
  ~SharedLibsLoader();

  std::string name() const override { return "shared-libs"; }

  void load(DiscordFramework::PluginInfo *info) override;

private:
  std::map<std::string, std::unique_ptr<Plugin>> m_plugins;

  std::map<std::string, std::unique_ptr<DLOpen>> m_plugins_native;

  void load(const char *path, Bot *bot);
};

ENTRYPOINT(SharedLibsLoader)
