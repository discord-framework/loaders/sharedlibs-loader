#include "sharedlibs-loader.h"

SharedLibsLoader::SharedLibsLoader(DiscordFramework::Bot *bot) : Loader(bot) {}

SharedLibsLoader::~SharedLibsLoader() {}

void SharedLibsLoader::load(DiscordFramework::PluginInfo *info) {
  DLOpen *library = new DLOpen(info->path.string());

  auto plugin_init = library->load<plugin_initfunctype *>("init_plugin");

  if (plugin_init != nullptr) {
    auto plugin = plugin_init(this->m_bot, info);

    // Add to native list
    this->m_plugins_native.emplace(info->name, library);
    // Add the plugin to the loader
    this->m_plugins.emplace(info->name, plugin);
  } else {
    throw std::runtime_error("Symbol \"init_plugin\" not found");
  }
}
