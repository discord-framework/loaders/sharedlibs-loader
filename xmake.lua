add_rules("mode.debug", "mode.release")

add_repositories("local-repo xmake/repo")

includes("./xmake/scripts/boot-libs.lua")

set_project("DF-sharedlibs-loader")

-- =========================
-- Library main config
-- =========================
target("sharedlibs-loader")
    set_kind("shared")

    set_languages("cxx20")
    add_cxflags("-fPIC -rdynamic", { force = true })
    add_ldflags("-rdynamic")

    set_version("1.1.0")

    add_files("src/**.cpp")
    add_headerfiles("src/**.h")
    add_includedirs("include/")

    load_packages()

    if (is_mode("debug")) then
        set_warnings("all", "error")
    end

    load_autoformat()

    set_installdir("./export")

    before_install(function(target)
            os.rm(target:installdir())
    end)

    after_install(function(target)
            os.mv(target:installdir().."/lib/libsharedlibs-loader.so", target:installdir())
            os.rm(target:installdir().."/lib")
            os.rm(target:installdir().."/include")
    end)
